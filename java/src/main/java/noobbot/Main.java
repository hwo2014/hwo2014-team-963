package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Main {
	static MessageLoop main;
	
    public static void main(String... args) {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port);

		try {
			final Socket socket = new Socket(host, port);
			final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

			main = new MessageLoop(reader, writer, botName, botKey);
			main.start();
		} catch (IOException e) {
			System.out.println("Failed to connect to server, exiting");
		}
    }
}
