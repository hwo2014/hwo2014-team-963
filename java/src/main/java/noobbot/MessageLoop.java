package noobbot;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import noobbot.message.*;

public class MessageLoop {
	final Gson gson = new Gson();
    final PrintWriter writer;
	final BufferedReader reader;
	final String botName;
	final String botKey;
	
	public MessageLoop(final BufferedReader reader, final PrintWriter writer,
			String botName, String botKey)
	{
		this.reader = reader;
		this.writer = writer;
		this.botName = botName;
		this.botKey = botKey;
	}

    public void start() throws IOException {
        String line;
		
		System.out.println("Joining race as " + botName + "/" + botKey);
        send(new Join(botName, botKey));

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			System.out.print("" + msgFromServer.gameTick + ":" + msgFromServer.msgType + " ");
			switch (msgFromServer.msgType) {
				case "carPositions":
					send(new Throttle(0.65));
					break;
				case "join":
					break;
				case "lapFinished":
					try {
						Time lapTime = gson.fromJson(gson.toJson(msgFromServer, msgFromServer.data.getClass()), Time.class);
						System.out.println("Lap time: " + lapTime.millis.doubleValue()/1000);
					} catch(Exception e) {
						System.out.println("Failed to parse: " + msgFromServer.data);
						e.printStackTrace();
					}
				case "gameInit":
					break;
				case "gameEnd":
					break;
				case "gameStart":
					break;
				default:
					System.out.println("Unrecognized message type: " + msgFromServer.msgType);
					send(new Ping());
					break;
			}
			System.out.println();
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg);
        writer.flush();
    }
}