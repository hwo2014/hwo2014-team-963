package noobbot.message;

public class LapFinished {
	public final Car car;
	public final Time lapTime;
	public final Time raceTime;
	public final Rank ranking;

	public LapFinished(Car car, Time lapTime, Time raceTime, Rank ranking) {
		this.car = car;
		this.lapTime = lapTime;
		this.raceTime = raceTime;
		this.ranking = ranking;
	}
}