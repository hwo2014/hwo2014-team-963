package noobbot.message;

public class Time {
	public final Integer lap;
	public final Integer ticks;
	public final Integer millis;

	public Time(Integer lap, Integer ticks, Integer millis) {
		this.lap = lap;
		this.ticks = ticks;
		this.millis = millis;
	}
	
	
}
