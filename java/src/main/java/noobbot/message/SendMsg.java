package noobbot.message;

import com.google.gson.Gson;

public abstract class SendMsg {
	@Override
    public String toString() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}