package noobbot.message;

public class Car {
	public final String name;
	public final String color;

	public Car(String name, String color) {
		this.name = name;
		this.color = color;
	}
}