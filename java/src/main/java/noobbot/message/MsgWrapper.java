package noobbot.message;

import com.google.gson.Gson;

public class MsgWrapper {
    public final String msgType;
    public final Object data;
	public final String gameId;
	public final Integer gameTick;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
		gameId = null;
		gameTick = null;
    }
	
	MsgWrapper(final String msgType, final Object data, final Integer gameTick) {
        this.msgType = msgType;
        this.data = data;
		this.gameId = null;
		this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
	
	public MsgWrapper(final SendMsg sendMsg, final Integer gameTick) {
		this(sendMsg.msgType(), sendMsg.msgData(), gameTick);
	}
}