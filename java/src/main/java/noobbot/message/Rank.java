package noobbot.message;

public class Rank {
	public final Integer overall;
	public final Integer fastestLap;

	public Rank(Integer overall, Integer fastestLap) {
		this.overall = overall;
		this.fastestLap = fastestLap;
	}
	
	
}
